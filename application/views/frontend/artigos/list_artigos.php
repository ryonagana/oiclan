



<div class="row">

	<div class="col-md-9 col-sm-12">
		<div class="row">
			<div class="box_container">
				<div  class="tab_titulo">
					<span class="tab_texto">Notícias</span>
					</div>
			<div class="artigo-box">

		<?php
		foreach($artigos as $m){ ?>
			<div class="sm-12 md-12 sm-12 columns">
					<article class="artigo-clan">
						<a href="<?php echo site_url('artigos/' . $m->idnoticia); ?>"><h2><?php echo escape_text($m->titulo);  ?></h2></a>

						<p>
							<em><?php echo show_autor($m);  ?></em>
						</p>
						<span class="artigo-texto">
							<?php echo escape_text($m->texto_pequeno); ?>
						</span>
					</article>
			</div>
		<?php } ?>
				</div>
		</div>
	</div>
	</div>

	<div class="col-md-3 col-sm-12">

		<div class="box_container">
			<div class="tab_titulo">
				<span class="tab_texto">Clanfrontos</span>
			</div >
			<div class="artigo-box">
				<?php echo $tpl_cf; ?>
			</div>
		</div>
	</div>

</div>
<div class="row">
	<div class="sm-12 md-12 sm-12 columns">
		<div class="box_container">
			<div class="tab_titulo">
				<span class="tab_texto">Últimas Screenshots</span>
			</div >
		<div class="artigo-box">
			<?php echo $tpl_screens; ?>
		</div>
		</div>
	</div>
</div>
