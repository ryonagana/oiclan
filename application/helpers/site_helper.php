<?php

function hide_email($email){ 
	$character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

 	$key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);

 	for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
 		$script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
 		$script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
 		$script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
 		$script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")"; 
 		$script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
 		return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;
}


function show_autor($obj){
	return empty($obj) ? '' : sprintf("Oi! %s", $obj->nick);
}

function show_data_publicacao($obj){
	return empty($obj) ? '' : sprintf("%s", $obj->data_publicacao);
}



function escape_text($str){
	return htmlspecialchars($str, ENT_QUOTES, "UTF-8");
}

function clanfrontos_titulo($obj){
	if(!$obj) return;

	$clan_vs = $obj->clan_vs;
	$result_vs = intval($obj->resultado_vs);
	$has_screenshot = !empty($obj->screenshot) ? true : false;
	$oi_result  = intval($obj->resultado_oi);

	$oi_class = 'cf_res';
	$vs_class = 'cf_res';

	$result_vs = $result_vs  <= 99 ? $result_vs : 99;
	$oi_result = $oi_result  <= 99 ? $oi_result : 99;

	if($result_vs > $oi_result){
		$oi_class = 'cf_res_lose';
		$vs_class = 'cf_res';
	}else {
		$oi_class = 'cf_res';
		$vs_class = 'cf_res_lose';
	}


	$msg = '<strong>Oi!</strong> <span class="%s">%d</span> vs. <span class="%s">%d</span> <strong>%s</strong>';
	$res = sprintf($msg, $oi_class, $oi_result, $vs_class, $result_vs, $clan_vs);


	return $res;
}

function clanfronto_resultado($obj){
		if(!$obj) return;

		$clan_vs = $obj->clan_vs;
		$result_vs = intval($obj->resultado_vs);
		$has_screenshot = !empty($obj->screenshot) ? true : false;
		$oi_result  = intval($obj->resultado_oi);

		$oi_class = 'cf_res';
		$vs_class = 'cf_res';

		$result_vs = $result_vs  <= 99 ? $result_vs : 99;
	$oi_result = $oi_result  <= 99 ? $oi_result : 99;

		if($result_vs > $oi_result){
			$oi_class = 'cf_res_lose';
			$vs_class = 'cf_res';
		}else {
			$oi_class = 'cf_res';
			$vs_class = 'cf_res_lose';
		}

	if(!$has_screenshot || empty($obj->screenshot) ){
		$msg = '<strong>Oi!</strong> <span class="%s">%d</span> vs. <span class="%s">%d</span> <strong>%s</strong>';
	}else {
		$msg = '<strong>Oi!</strong> <span class="%s">%d</span> vs. <span class="%s">%d</span> <strong>%s</strong> - <a href="">Screenshot</a>';
		
	}
	
	$res = sprintf($msg, $oi_class, $oi_result, $vs_class, $result_vs, $clan_vs);
	

	return $res;

}

function template_footer(){
	$CI =& get_instance();
	return $CI->load->view('frontend/base/bottom.php');
}
