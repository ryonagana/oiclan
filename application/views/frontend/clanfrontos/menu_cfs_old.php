<?php

$total_registros = count($lista_cf) % 4;

$threeCols = $total_registros == 0 ? true : false;

?>



<div class="box_container">
	<div  class="tab_titulo">
		<span class="tab_texto">Lista de Clanfrontos</span>
	</div>
	<div class="artigo-box">

		<?php
		if($threeCols){

		?>
		<div class="row">
			<?php foreach($lista_cf as $c){ ?>
			<div class="col-lg-3 col-md-3 col sm-12">
					<div class="box_container">
						<div class="tab_titulo"><?php echo clanfrontos_titulo($c); ?></div>
						<div class="artigo-box">
							<?php if( !empty($c->screenshot) ){ ?>
							<a href="<?php echo base_url('screens/image/' . $c->screenshot); ?>"  data-fancybox data-caption="">
									<img class="img-thumbnail"  src="<?php echo base_url('screens/thumb/' . $c->screenshot); ?>" alt="">
								</a>
							<?php }else { ?>
									SEM IMAGEM
							<?php } ?>
						</div>
					</div>
			</div>
			<?php } ?>

		</div>
		<?php
			}else { ?>


		<div class="row">

			<?php foreach($lista_cf as $c){  ?>
			<div class="col-lg-4 col-md-4 col sm-12">
				<div class="box_container">
					<div class="tab_titulo"><?php echo clanfrontos_titulo($c); ?></div>
					<div class="artigo-box">
						<?php if( !empty($c->screenshot) ){ ?>
						<a href="<?php echo base_url('screens/image/' . $c->screenshot); ?>"  data-fancybox data-caption="">
							<img class="img-thumbnail"  src="<?php echo base_url('screens/thumb/' . $c->screenshot); ?>" alt="">
						</a>
						<?php }else { ?>
						<img src="<?php echo base_url('screens/thumb/no_ss.jpg'); ?>" alt="" class="img-thumbnail">
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>

		</div>
		<?php	} 	?>

	</div>
</div>
