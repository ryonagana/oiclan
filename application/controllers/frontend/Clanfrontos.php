<?php
	class Clanfrontos extends MY_Controller {

		private $count_cfs;

		public function __construct(){
			parent::__construct();

			$this->load->model('frontend/Clanfrontos_model');
			$this->load->model('frontend/Screenshots_model');


			$this->load->library('pagination');

			$config['base_url'] = site_url('cf/pagina');
			$config['uri_segment'] = 3;
			$config['total_rows'] = $this->Clanfrontos_model->count_cfs();
			$config['per_page'] = 6;
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = 'Ultimo';
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] = '</ul>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';



			$config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Anterior';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';


			$config['next_link'] = 'Proximo<i class="fa fa-long-arrow-right"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';


			$this->pagination->initialize($config);

			$this->count_cfs = $config['per_page'];

		}

		public function index(){






			$cfs = $this->Clanfrontos_model->list_cfs($this->count_cfs);

			$this->loadHeader("Clanfrontos");
			$this->menu();
			$this->load->view('frontend/clanfrontos/menu_cfs', array('lista_cf' => $cfs, 'paginas' => $this->pagination->create_links()));
			$this->loadFooter();
		}

		public function pagina($offset = 1){


			$cfs = $this->Clanfrontos_model->list_cfs($this->count_cfs, $offset);

			$this->loadHeader("Clanfrontos");
			$this->menu();
			$this->load->view('frontend/clanfrontos/menu_cfs', array('lista_cf' => $cfs, 'paginas' => $this->pagination->create_links()));
			$this->loadFooter();

		}

	}
