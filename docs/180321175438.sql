/*
MySQL Backup
Source Server Version: 5.5.59
Source Database: oiclan
Date: 21/03/2018 17:54:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `oi_album`
-- ----------------------------
DROP TABLE IF EXISTS `oi_album`;
CREATE TABLE `oi_album` (
  `idalbum` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `fk_login` int(11) NOT NULL,
  PRIMARY KEY (`idalbum`,`fk_login`),
  KEY `fk_oi_album_oi_login1_idx` (`fk_login`) USING BTREE,
  CONSTRAINT `oi_album_ibfk_1` FOREIGN KEY (`fk_login`) REFERENCES `oi_login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `oi_arquivo`;
CREATE TABLE `oi_arquivo` (
  `idarquivo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `fk_cat_arquivo` int(11) NOT NULL,
  `login_fk` int(11) NOT NULL,
  PRIMARY KEY (`idarquivo`,`fk_cat_arquivo`,`login_fk`),
  KEY `fk_oi_arquivo_oi_cat_arquivo1_idx` (`fk_cat_arquivo`) USING BTREE,
  KEY `fk_oi_arquivo_oi_login1_idx` (`login_fk`) USING BTREE,
  CONSTRAINT `oi_arquivo_ibfk_1` FOREIGN KEY (`fk_cat_arquivo`) REFERENCES `oi_cat_arquivo` (`idcat_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `oi_arquivo_ibfk_2` FOREIGN KEY (`login_fk`) REFERENCES `oi_login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_cat_arquivo`
-- ----------------------------
DROP TABLE IF EXISTS `oi_cat_arquivo`;
CREATE TABLE `oi_cat_arquivo` (
  `idcat_arquivo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`idcat_arquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_clanfrontos`
-- ----------------------------
DROP TABLE IF EXISTS `oi_clanfrontos`;
CREATE TABLE `oi_clanfrontos` (
  `idclanfrontos` int(11) NOT NULL AUTO_INCREMENT,
  `clan_vs` varchar(18) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '0',
  `resultado_oi` tinyint(4) DEFAULT NULL,
  `resultado_vs` tinyint(4) DEFAULT NULL,
  `screenshot` varchar(127) DEFAULT NULL,
  `fk_login` int(11) NOT NULL,
  PRIMARY KEY (`idclanfrontos`,`fk_login`),
  KEY `fk_oi_clanfrontos_oi_login1_idx` (`fk_login`) USING BTREE,
  CONSTRAINT `oi_clanfrontos_ibfk_1` FOREIGN KEY (`fk_login`) REFERENCES `oi_login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_login`
-- ----------------------------
DROP TABLE IF EXISTS `oi_login`;
CREATE TABLE `oi_login` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `passwd` varchar(64) NOT NULL,
  `ultimo_login` datetime DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_membro`
-- ----------------------------
DROP TABLE IF EXISTS `oi_membro`;
CREATE TABLE `oi_membro` (
  `idmembros` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `sobrenome` varchar(60) DEFAULT NULL,
  `nick` varchar(25) DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '0',
  `arma_favorita` varchar(45) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `lider` tinyint(1) DEFAULT '0',
  `foto` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`idmembros`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_membro_login`
-- ----------------------------
DROP TABLE IF EXISTS `oi_membro_login`;
CREATE TABLE `oi_membro_login` (
  `oi_membro_idmembros` int(11) NOT NULL,
  `oi_login_idlogin` int(11) NOT NULL,
  PRIMARY KEY (`oi_membro_idmembros`,`oi_login_idlogin`),
  KEY `fk_oi_membro_has_oi_login_oi_login1_idx` (`oi_login_idlogin`) USING BTREE,
  KEY `fk_oi_membro_has_oi_login_oi_membro1_idx` (`oi_membro_idmembros`) USING BTREE,
  CONSTRAINT `oi_membro_login_ibfk_1` FOREIGN KEY (`oi_login_idlogin`) REFERENCES `oi_login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `oi_membro_login_ibfk_2` FOREIGN KEY (`oi_membro_idmembros`) REFERENCES `oi_membro` (`idmembros`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_news_tag`
-- ----------------------------
DROP TABLE IF EXISTS `oi_news_tag`;
CREATE TABLE `oi_news_tag` (
  `idnews_tag` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idnews_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_noticia`
-- ----------------------------
DROP TABLE IF EXISTS `oi_noticia`;
CREATE TABLE `oi_noticia` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(127) DEFAULT NULL,
  `slug` varchar(127) DEFAULT NULL,
  `texto_pequeno` varchar(255) DEFAULT NULL,
  `texto` text,
  `data_pub` datetime DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `login_fk` int(11) NOT NULL,
  PRIMARY KEY (`idnoticia`,`login_fk`),
  KEY `fk_oi_noticia_oi_login1_idx` (`login_fk`) USING BTREE,
  CONSTRAINT `oi_noticia_ibfk_1` FOREIGN KEY (`login_fk`) REFERENCES `oi_login` (`idlogin`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_noticia_has_tag`
-- ----------------------------
DROP TABLE IF EXISTS `oi_noticia_has_tag`;
CREATE TABLE `oi_noticia_has_tag` (
  `fk_noticia` int(11) NOT NULL,
  `fk_tag_noticia` int(11) NOT NULL,
  PRIMARY KEY (`fk_noticia`,`fk_tag_noticia`),
  KEY `fk_oi_noticia_has_oi_news_tag_oi_news_tag1_idx` (`fk_tag_noticia`) USING BTREE,
  KEY `fk_oi_noticia_has_oi_news_tag_oi_noticia1_idx` (`fk_noticia`) USING BTREE,
  CONSTRAINT `oi_noticia_has_tag_ibfk_1` FOREIGN KEY (`fk_tag_noticia`) REFERENCES `oi_news_tag` (`idnews_tag`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `oi_noticia_has_tag_ibfk_2` FOREIGN KEY (`fk_noticia`) REFERENCES `oi_noticia` (`idnoticia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `oi_screenshots`
-- ----------------------------
DROP TABLE IF EXISTS `oi_screenshots`;
CREATE TABLE `oi_screenshots` (
  `idscreens` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(127) DEFAULT NULL,
  `descricao` tinytext,
  `oi_screenshotscol` varchar(45) DEFAULT NULL,
  `fk_album` int(11) NOT NULL,
  PRIMARY KEY (`idscreens`,`fk_album`),
  KEY `fk_oi_screenshots_oi_album1_idx` (`fk_album`) USING BTREE,
  CONSTRAINT `oi_screenshots_ibfk_1` FOREIGN KEY (`fk_album`) REFERENCES `oi_album` (`idalbum`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `oi_album` VALUES ('1','Clanfrontos','1');
INSERT INTO `oi_clanfrontos` VALUES ('1','[TA]','1','10','6','','1'), ('3','[TA]','1','10','5','','1'), ('4','[TA]','1','10','4','ta_vs_oi01.jpg','1'), ('5','[TA]','1','10','9','ta_vs_oi02.jpg','1'), ('6','[CVN]','1','10','7','cvn_vs_oi01.jpg','1'), ('7','[CVN]','1','10','8','cvn_vs_oi02.jpg','1');
INSERT INTO `oi_login` VALUES ('1','ryonagana','cb3252','2017-04-25 18:02:43','1'), ('2','venta','biba','2017-07-19 12:32:53','1');
INSERT INTO `oi_membro` VALUES ('1','Nicholas ','Oliveira','CyBEr','1','MP5, Sniper','','0','cyber.jpg'), ('2','Sergio ','Oliveira','Ventan!a','1','M4, Sniper, Mp5','','1','venta.jpg'), ('3','Bruno',NULL,'Zippy','0','M4',NULL,'0','zippy.jpg'), ('4','Jansen','Farsura','Abduzido','1','M4, Sniper',NULL,'0','abdu.jpg'), ('5','Tiago',NULL,'F!sh','0','Sniper',NULL,'0',NULL), ('6','Bruno','Torres','Spectrum','0','Sniper',NULL,'0',NULL), ('7','Jader','Farsura','HellsX','1','Sniper',NULL,'0','hell.jpg'), ('8','Luan','Bieber','Luanzito','0','Sniper',NULL,'0','luan.jpg'), ('9','Giu','Knox','Z3ro','1','Sniper, M4',NULL,'0','zzero.jpg'), ('10','Edson','Reis','Vaca','1','Sniper',NULL,'0','vaca.jpg'), ('11','John','Loco','Jono','1','Sniper',NULL,'0','jono.jpg'), ('12','Yan','Marques','Yaniu','1','Sniper',NULL,'0','yaniu.jpg'), ('13','João','Neto','Scott','1','M4, Sniper',NULL,'0','scott.jpg'), ('14','Toshi','Yoritomi','Toshi','1','sniper',NULL,'0','toshi.jpg'), ('15','Fernando','Preiss','Preiss','1','Sniper',NULL,'0','preiss.jpg'), ('16','Ricardo',NULL,'R3T4.MV','1','M4 ',NULL,'0','reta.jpg'), ('17','Cassyo','Hanke','Cassyo','0','Sniper',NULL,'0','cassyo.jpg'), ('18','Bruno','Emmerick','ErroR','1','Sniper e M4',NULL,'0','error.jpg'), ('19','Bruno',NULL,'Terz','0','M4,Sniper',NULL,'0','terz.jpg'), ('20','Paulo','Vinicius','Dushy','0','Sniper, M4',NULL,'0','dushy.jpg'), ('21','Nelson','Regailo','N3wlong','0','Sniper,M4,MP5',NULL,'0','newlong.jpg'), ('22','Luiz','Paulo','ChilD','1','Sniper',NULL,'0','child.jpg');
INSERT INTO `oi_membro_login` VALUES ('1','1'), ('2','2');
INSERT INTO `oi_news_tag` VALUES ('1','action-quake-2','action-quake-2'), ('2','cf','clanfronto'), ('3','patos','patos');
INSERT INTO `oi_noticia` VALUES ('1','Inauguração do Site do Oi','ola-a-toldos','Ola a todos! Estamos re inaugurando o site totalmente refeito, a parte interna esta andando aos poucos e estamos fazendo o maximo possivel para que tudo termine no tempo mais curto, apesar que eu (cyber) estou focando nesse site somente nas horas livres.','Ola a todos! Estamos re inaugurando o site totalmente refeito, a parte interna esta andando aos poucos, pois estamos correndo com o desenvolvimento para deixar o site usavel, algumas seções ainda não estão feitas, por isso peço perdão, mas no futuro tera uma secao para servir arquivos do nosso clã, como paks e cfgs. quem quiser cheat conversar com os caras do RMK','2017-07-19 00:00:00','1','1'), ('2','Novidades sobre o Site!','noticias-quentinhas','em breve o site vai ter screenshots  e imagens das antigas, aguardem em breve vao ser adicionadas novas infos de CF e a area de arquivos será inaugurada','em breve o site vai ter screenshots  e imagens das antigas, aguardem em breve vao ser adicionadas novas infos de CF e a area de arquivos será inaugurada','2017-08-23 13:00:00','1','1');
INSERT INTO `oi_screenshots` VALUES ('1','quake000.jpg','Clanfronto contra o clã [TA]',NULL,'1'), ('2','quake001.jpg','alguem pedindo mapa lixo',NULL,'1'), ('3','quake002.jpg','CF com [TA]',NULL,'1'), ('4','quake003.jpg','Oi rulando as tetas',NULL,'1');
