<div class="submenu">
	<h1>Membros</h1>
	<span>Membros do clã</span>
</div>

<div class="container">

<div class="row">
<?php foreach($membros as $membro){ ?>

	<div class="col-sm-4 col-lg-4 col-md-4">
		<div class="box-membro">
			<article class="membro">

				<h3 class="membro-nome">Oi!<?php echo $membro->nick; ?></h3>
				<?php if(!$membro->foto ||  !file_exists('assets/images/membros/final/' . $membro->foto)){ ?>
				<img src="<?php echo base_url('assets/images/semfoto.png'); ?>" alt="" class="membro-foto">
				<?php }else { ?>
				<img src="<?php echo base_url('assets/images/membros/final/' . $membro->foto ); ?>" alt="" class="membro-foto">
				<?php } ?>
				<p><span>Nome:</span> <?php echo $membro->nome; ?> <?php echo $membro->sobrenome; ?>  </p>
				<p><span>Arma:</span> <?php echo $membro->arma_favorita; ?> </p>

				<?php $class = $membro->ativo == 2 ? 'membro-inativo' : 'membro-ativo'; ?>

				<p><span>Status:</span> <span class="<?php echo $class; ?>"><?php  echo $membro->ativo == 2 ? 'Inativo' : 'Ativo';  ?></span></p>
			</article>
		</div>
	</div>



<?php } ?>
</div>
</div>
