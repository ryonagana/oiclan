<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.html">Oi Clan Administração</a>
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">


		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
			<ul class="dropdown-menu">

				<li>
					<a href="#"><i class="fa fa-fw fa-gear"></i> Configuracoes</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="#"><i class="fa fa-fw fa-power-off"></i> Sair</a>
				</li>
			</ul>
		</li>
	</ul>
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li class="active">
				<a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
			</li>

			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#c_noticia"><i class="fa fa-fw fa-arrows-v"></i>Notícias <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="c_noticia" class="collapse">
					<li>
						<a href="#">Adicionar / Remover</a>
					</li>
				</ul>
			</li>

			<li>
				<a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Screenshots</a>
			</li>
			<li>
				<a href="tables.html"><i class="fa fa-fw fa-table"></i> Clanfrontos</a>
			</li>
			<li>
				<a href="forms.html"><i class="fa fa-fw fa-edit"></i> Arquivos</a>
			</li>

			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#c_usuario"><i class="fa fa-fw fa-arrows-v"></i>Usuarios <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="c_usuario" class="collapse">
					<li>
						<a href="#">Configurar</a>
					</li>
					<li>
						<a href="#">Adicionar / Remover</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>
