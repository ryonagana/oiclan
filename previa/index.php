<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Os Individuos Clan AQ2</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<link rel="stylesheet" href="assets/css/theme.css?v=<?php echo rand(0,30.0); ?>">
		<link rel="stylesheet" href="assets/css/base.css">


		<script
				src="https://code.jquery.com/jquery-3.2.1.min.js"
				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				crossorigin="anonymous"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="assets/js/vegas/vegas.min.css">
		<script src="assets/js/vegas/vegas.min.js"></script>
		<script src="assets/js/preload_sound.js"></script>

	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="titulo_h1">EM BREVE: Site do Clan Os Individuos</div>
					</div>
			</div>
		</div>
		<script>


			$("body").vegas({
				shuffle: true,
				align: 'center',
				valign: 'center',
				slides: [
					{ src: 'assets/images/photo_1.jpg' },
					{ src: 'assets/images/photo_2.jpg' },
					{ src: 'assets/images/img3.jpg' },
					{ src: 'assets/images/img4.jpg' }
				],
				overlay: 'http://servidor2.dev:8080/oiclan/assets/js/vegas/overlays/01.png'
			});
		</script>
	</body>
