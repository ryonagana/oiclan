<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php if(!$title){ ?>
	<title>Os Individuos Clan AQ2</title>
	<?php }else { ?>
	<title>Os Individuos Clan AQ2 - <?php echo $title; ?></title>
	<?php } ?>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('apple-touch-icon.png'); ?> ">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('favicon-32x32.png'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon-16x16.png') ?>">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/base.css"); ?>">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

	<script
			src="https://code.jquery.com/jquery-3.2.1.min.js"
			integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			crossorigin="anonymous"></script>
			
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" integrity="sha256-7TyXnr2YU040zfSP+rEcz29ggW4j56/ujTPwjMzyqFY=" crossorigin="anonymous" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js" integrity="sha256-wzoq0P7w2qLzRcJsF319y6G9Oilk3nU5CZ8tnY9TjFI=" crossorigin="anonymous"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<link rel="stylesheet" href="<?php echo  base_url('assets/js/vegas/vegas.min.css') ; ?>" ?>
	<script src="<?php echo base_url('assets/js/vegas/vegas.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/preload_sound.js'); ?>"></script>
	
	<?php if (!$title) { ?>
	<meta property="og:title" content="Os Indivíduos Clan" />
	<?php }else { ?>
	<meta property="og:title" content="Os Indivíduos Clan - <?php $title ?>" />
	<?php } ?>
	<meta property="og:url" content="<?php echo current_url(); ?>" />
	<meta property="og:site_name" content="Os Indivíduos" />
	<meta property="og:image" content="<?php echo base_url('assets/images/oilogo.png');  ?>" />

</head>
<body>


	
