<?php

?>
<div class="box_container">
	<div  class="tab_titulo">
		<span class="tab_texto">Screenshots</span>
	</div>
	<div class="artigo-box">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php for($i = 0; $i < ($total % 3) ; $i++){ ?>
				<div class="row">

					<?php foreach($screens as $s){ ?>
					<div class="col-md-4 col-sm-12">
						<a href="<?php echo base_url('screens/image/' . $s->imagem); ?>" data-fancybox data-caption="<?php echo $s->descricao; ?>">
							<img class="img-thumbnail" src="<?php echo base_url('screens/thumb/' . $s->imagem); ?>" alt="">
						</a>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
