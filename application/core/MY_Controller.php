<?php 


class MY_Controller extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('cookie');
		
	}
	
	


	public function menu(){
		$this->load->view("frontend/base/top.php");
	}
	public function erro(){
		$this->output->set_status_header('404');
		$this->load->view('frontend/base/404');
	}
	
	public function loadHeader($title = NULL){
		$this->load->view("frontend/base/header", array('title' => $title));
	}
	
	public function loadFooter(){
		//$this->load->view('frontend/base/bottom');
		$this->load->view("frontend/base/footer");
	}
	
}


class MY_BackendController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('cookie');

	}

	public function loadHeader($title = NULL){
		$this->load->view("backend/base/header", array('title' => $title));
	}

	public function loadFooter(){
		$this->load->view("backend/base/footer");
	}

}
