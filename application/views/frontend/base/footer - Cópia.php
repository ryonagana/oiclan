
<script>
	$("body").vegas({
		preload: false,
		preloadImage: true,
		preloadVideo: true,
		shuffle: true,
		align: 'left',
		valign: 'top',
		cover: true,

		slides: [
			{ src: '<?php echo base_url('assets/images/photo_1.jpg'); ?>' },
			{ src: '<?php echo base_url('assets/images/photo_2.jpg'); ?>' },
			{ src: '<?php echo base_url('assets/images/img3.jpg'); ?>' },
			{ src: '<?php echo base_url('assets/images/img4.jpg'); ?>' }
		],
		overlay: '<?php echo base_url('assets/js/vegas/overlays/01.png'); ?>'
	});
</script>



</body>
</html>
