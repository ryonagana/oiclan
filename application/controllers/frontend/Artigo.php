<?php 	

class Artigo extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('frontend/Artigos_model');
	}
	
	public function index($id = NULL){
		$this->loadHeader();
		$this->menu();
		
		$artigo = $this->Artigos_model->getSingleArticleId($id);

		if($id != NULL || !empty($artigo)){


			$this->load->view('frontend/artigos/mostra_artigo.php', array('artigo' => $artigo ) );
		}
		$this->loadFooter();
	}
	
	public function sobre(){
		$this->loadHeader();
		$this->menu();
		$this->loadFooter();

	}
}
