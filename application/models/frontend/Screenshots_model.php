<?php

class Screenshots_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getAllScreens($albumid = NULL){
		$this->db->select('*');
		$this->db->from('oi_album');
		$this->db->join('oi_screenshots', 'oi_album.idalbum = oi_screenshots.fk_album', 'outer join');
		if($albumid){
			$this->db->where('idalbum', $albumid);
		}
		$q = $this->db->get();
		return $q;
	}
	
	public function getRandomScreenshots($max = 10){
		$this->db->select('*');
		$this->db->from('oi_screenshots');
		$this->db->order_by('title', 'RANDOM');
		$this->db->limit($max);
		
		return $this->db->get();
		
	}
	
}