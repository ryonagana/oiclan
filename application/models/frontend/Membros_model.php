<?php

class Membros_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function listarMembros(){
		$this->db->order_by('lider', 'DESC');
		//$query = $this->db->get_where('oi_membro', array('ativo' => 1, 'ativo' => 2 ));
		$query = $this->db->query("
			SELECT idmembros, nome, sobrenome, arma_favorita, nick, ativo, lider, foto
			FROM oi_membro
			WHERE ativo >= 1
			ORDER BY lider DESC

		");
		return $query->result();
	}
	
	public function detalhesMembro($id){
		$q = $this->get_where('oi_membro', array('ativo' => 1, 'idmembros' => intval($id)));
		return $q->result();
		
		
	}
	
}
