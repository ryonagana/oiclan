<?php


class Dashboard extends MY_BackendController {

	public function __construct(){
		parent::__construct();
	}


	public function index(){
		$this->loadHeader();
		$this->load->view('backend/base/menu');
		$this->load->view('backend/base/content');
		$this->loadFooter();
	}


}
