<?php
class Screenshots extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('frontend/Screenshots_model');
	}

	public function index(){
		
		$this->loadHeader("Screenshots");
		$this->menu();
		
		$screens = $this->Screenshots_model->getAllScreens();
		
	
		
		$this->load->view('frontend/screenshots/listar_screens', array('screens' => $screens->result(), 'total' => $screens->result_id->num_rows ));
		$this->loadFooter();
	}
}
