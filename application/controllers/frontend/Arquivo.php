<?php 	

class Arquivo extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}

	public function index($id = NULL){
		$this->loadHeader();
		$this->menu();
		$this->load->view('frontend/arquivos/lista_arquivos');
		$this->loadFooter();
	}
}
