<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('apple-touch-icon.png'); ?> ">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('favicon-32x32.png'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon-16x16.png') ?>">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/sopranos/stylesheet.css'); ?>">
	
	<link rel="stylesheet" href="<?php echo base_url('assets/css/craftyslide/css/craftyslide.css'); ?>">
	<link rel="stylesheet" href="<?php echo  base_url('assets/js/vegas/vegas.min.css') ; ?>" ?>
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/brands.css" integrity="sha384-IiIL1/ODJBRTrDTFk/pW8j0DUI5/z9m1KYsTm/RjZTNV8RHLGZXkUDwgRRbbQ+Jh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">

	

	<script 
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
			
	<script src="<?php echo base_url('assets/js/vegas/vegas.js') ?>"></script>
			
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	
	<script src="<?php echo base_url('assets/css/craftyslide/js/craftyslide.min.js'); ?>">
	</script>

	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/brands.js" integrity="sha384-sCI3dTBIJuqT6AwL++zH7qL8ZdKaHpxU43dDt9SyOzimtQ9eyRhkG3B7KMl6AO19" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/css/app.css'); ?>">
	
	<?php if(!isset($title)){ ?>
	<title>Os indivíduos Action Quake 2 Clan</title>
	<?php }else { ?>
	<title>Os indivíduos Action Quake 2 Clan - <?php echo $title; ?></title>	
	<?php } ?>

	<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
</head>
<body>


	

	<div class="container">
		
