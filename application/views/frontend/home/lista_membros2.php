<div class="box_container">
<div  class="tab_titulo">
	<span class="tab_texto">Membros</span>
</div>
<div class="artigo-box">
	<div class="row">

	<?php foreach($membros as $m){ ?>



						<div class="col-lg-2 col-md-2 col-sm-12">

							<div class="img-membro">
								<?php if(!empty($m->foto)){ ?>
								<img width="190" height="106" src="<?php echo base_url('assets/images/membros/final/' . $m->foto ); ?>" class="membro-pic" alt="" />
								<?php } ?>
							</div>

						</div>
						<div class="col-lg-10 col-md-10 col-sm-12">


								<ul class="lista-membro">
									<li>
										<h3><a href="#<?php echo "membro-".$m->nick; ?>">Oi! <?php echo $m->nick; ?></a></h3>
									</li>
									<li>
										<span><strong>Nome:</strong> <?php echo $m->nome . ' ' . $m->sobrenome; ?></span>
									</li>
									<li>
										<span><strong>E-Mail: </strong><?php echo !empty($m->email) ? hide_email($m->email) : 'Sem E-mail'; ?></span>
									</li>
									<li><span><strong>Arma Favorita:</strong> <?php echo $m->arma_favorita; ?></span></li>
								</ul>
							</div>

	<?php  } ?>
	</div>
</div>
</div>
