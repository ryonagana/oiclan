<?php

class Artigos_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getArticles($limit=10,$offset=0){
		$total = $this->db->count_all_results('oi_noticia');

		if($offset > $total) $offset = $total;

		$this->db->query("SET lc_time_names = 'pt_BR'");
		$q = $this->db->query("

			SELECT
			noticia.idnoticia,
			noticia.titulo,
			noticia.slug,
			noticia.texto_pequeno,
			noticia.texto,
			(
				SELECT DATE_FORMAT(data_pub,'%W, %d de %M de %Y') FROM oi_noticia
			  WHERE oi_noticia.idnoticia = noticia.idnoticia
			) as data_publicacao,
			noticia.ativo,
			mem.nome,
			mem.sobrenome,
			mem.nick

			FROM oi_noticia noticia
			INNER JOIN oi_membro_login oim ON oim.oi_login_idlogin = noticia.login_fk
			INNER JOIN oi_membro mem ON mem.idmembros = oim.oi_membro_idmembros
			INNER JOIN oi_login  log ON log.idlogin = oim.oi_login_idlogin
		");
		$this->db->limit($limit, $offset, FALSE);

		//$q = $this->db->get('oi_noticia');
		//$this->db->join('oi_login', 'oi_login.fk_login = oi_noticia.idlogin');
		//$this->db->order_by('data_pub', 'ASC');
		return $q->result();
	}

	public function getSingleArticleId($id){


		$q = $this->db->query("

			SELECT
			noticia.idnoticia,
			noticia.titulo,
			noticia.slug,
			noticia.texto_pequeno,
			noticia.texto,
			(
				SELECT DATE_FORMAT(data_pub,'%W, %d de %M de %Y') FROM oi_noticia
			  WHERE oi_noticia.idnoticia = noticia.idnoticia
			) as data_publicacao,
			noticia.ativo,
			mem.nome,
			mem.sobrenome,
			mem.nick

			FROM oi_noticia noticia
			INNER JOIN oi_membro_login oim ON oim.oi_login_idlogin = noticia.login_fk
			INNER JOIN oi_membro mem ON mem.idmembros = oim.oi_membro_idmembros
			INNER JOIN oi_login  log ON log.idlogin = oim.oi_login_idlogin
			WHERE  noticia.idnoticia = ?
		", array(intval($id)));

		return $q->result();
	}

}
