<?php

$total = count($lista_cf);

$colunas  = $total <= 3 ? 6 : 4

?>

<div class="submenu">
	<h1>Clanfrontos!</h1>
	<span>Screenshots de CF's Realizados</span>
</div>






<div class="row">
<?php foreach($lista_cf as $cf){ ?>
	<div class="col-md-<?php echo $colunas ?> col-lg-<?php echo $colunas ?> col-sm-<?php echo $colunas ?>">
		<article class="site-cf">
			<h3>
				<?php echo clanfrontos_titulo($cf); ?>
			</h3>
			<?php if(!$cf->screenshot || !file_exists('screens/image/' . $cf->screenshot) ) { ?>
			<img src="<?php echo base_url('assets/images/no_ss.jpg'); ?>" alt="" class="img-thumbnail">
			<?php }else { ?>
			<a href="<?php echo base_url('screens/image/' . $cf->screenshot); ?>"  data-fancybox data-caption="">
				<img class="img-thumbnail"  src="<?php echo base_url('screens/thumb/' . $cf->screenshot); ?>" alt="">
			</a>
			<?php } ?>


		</article>

	</div>

<?php } ?>


<div class="col-md-12 col-sm-12">
	<?php echo $paginas;	?>
</div>

</div>
