<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('frontend/Artigos_model');
		$this->load->model('frontend/Clanfrontos_model');
		$this->load->model('frontend/Screenshots_model');
		
	}

	public function home_cfs(){

		$cfs = $this->Clanfrontos_model->list_cfs(4);

		return $this->load->view('frontend/clanfrontos/site_cfs', array('cfs' => $cfs) ,  true);
	}
	
	public function home_screenshots(){
		$screens = $this->Screenshots_model->getRandomScreenshots();
		return $this->load->view('frontend/screenshots/home_screens', array('screens' => $screens->result(),
																			  'total' =>  $screens->result_id->num_rows 
																			 ), true);
	}


	public function index()
	{
		//redirect("artigo");
		$this->loadHeader();
		$this->menu();

		$listar_artigos = $this->Artigos_model->getArticles();
		$tpl_cfs = $this->home_cfs();
		$tpl_screens = $this->home_screenshots();

		$this->load->view('frontend/artigos/listar_artigos2.php', array('artigos' => $listar_artigos,
																	 'tpl_cf' => $tpl_cfs,
																	 'tpl_screens' => $tpl_screens
						 ));
		$this->loadFooter();
	}
}
