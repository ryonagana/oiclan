<?php

class Clanfrontos_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function list_cfs($limit = 10, $offset = 0){;

		$this->db->limit($limit, $offset);
		$this->db->where('ativo', 1);
		$d = $this->db->get('oi_clanfrontos');

		return $d->result();
	}

	public function count_cfs(){
		$total = $this->db->count_all_results('oi_clanfrontos');  // Produces an integer, like 25
		$this->db->where('ativo', 1);
		$this->db->from('oi_clanfrontos');
		return $this->db->count_all_results(); //
	}

}
