<?php 

$total = count($screens);

$colunas  = $total <= 3 ? 6 : 4

?>

<div class="submenu">
	<h1>Screenshots</h1>
	<span>Screenshots aleatorias, algumas engraçadas</span>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<?php for($i = 0; $i < ($total % 3) ; $i++){ ?>
		<div class="row">

			<?php foreach($screens as $s){ ?>
			<div class="col-md-4 col-sm-12">
				<a href="<?php echo base_url('screens/image/' . $s->imagem); ?>" data-fancybox data-caption="<?php echo $s->descricao; ?>">
					<img class="img-thumbnail" src="<?php echo base_url('screens/thumb/' . $s->imagem); ?>" alt="">
				</a>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>
