<article class="news-box">
	<div class="news-topo">
		<h3>Notícias</h3>
	</div>
	<div class="news-body">
			<?php foreach($artigos as $artigo){ ?>
				<div class="artigo">
					<h4 class="artigo-titulo">
						<span><a href=""><?php echo $artigo->titulo; ?> </a></span>
					</h4>
					
					<div class="news-body">
					<p>
						<?php echo escape_text($artigo->texto_pequeno); ?>
					</p>
					<em>
						Publicado por:<?php echo show_autor($artigo); ?> em <?php echo show_data_publicacao($artigo); ?></em> 
					</div>
					
				</div>
			<?php } ?>
	</div>

</article>