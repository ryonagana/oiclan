'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat  = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var concatJs = require("gulp-concat-js");
var pump = require('pump');
var jslint = require('gulp-jslint');


gulp.task('sass', function () {
	return gulp.src('./sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'));
});

gulp.task('compress', function () {
	
	return gulp.src(['js/*.js'])
		.pipe(jslint({
			predef: [ 'a_global' ],
			global: [ 'a_global' ]
		}))
		.pipe(concat('dist/app.js'))
		.pipe(gulp.dest('dist'))
		.pipe(rename('app.js'))
		//.pipe(uglify())
		.pipe(gulp.dest('../assets/js'));


});

gulp.task('bundle', function () {
	return gulp.src(['css/**/*.css'])
		
		.pipe(concatCss("css/app.css"))
		.pipe(gulp.dest('../assets/'));
});



gulp.task('watch', function () {
	gulp.watch('sass/**/*.scss', ['sass']);
	gulp.watch('js/**/*.js', ['compress']);
	gulp.watch('css/**/*.css', ['bundle']);

});

gulp.task('default', [ 'compress', 'sass', 'bundle']);
